<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Type;

/**
 * Class ImageType
 *
 * @author Jan Małysiak <jan.a.malysiak@gmail.com>
 */
class ImageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class)
            ->add('height', IntegerType::class, [
                'constraints' => new Type(['type' => 'integer']),
            ])
            ->add('width', IntegerType::class, [
                'constraints' => new Type(['type' => 'integer']),
            ])
        ;
    }
}
