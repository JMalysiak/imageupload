<?php

namespace App\Controller;

use App\Form\ImageType;
use App\Service\Uploader;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UploadController
 *
 * @author Jan Małysiak <jan.a.malysiak@gmail.com>
 */
class UploadController extends Controller
{
    /**
     * @Route("/upload")
     */
    public function upload(Request $request, Uploader $uploader, LoggerInterface $logger)
    {
        $form = $this->createForm(ImageType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $fileName = $uploader->upload($data['file'], $data['height'], $data['width']);

            $size = $data['file']->getSize();
            $message = sprintf('%s, size: %s bytes, height: %s, width: %s', $fileName, $size, $data['height'], $data['width']);
            $logger->info($message);
        } else {
            $fileName = null;
        }

        return $this->render('upload.html.twig', [
            'form' => $form->createView(),
            'fileName' => $fileName,
        ]);
    }
}
