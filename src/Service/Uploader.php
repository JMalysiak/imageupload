<?php

namespace App\Service;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class Uploader
 *
 * @author Jan Małysiak <jan.a.malysiak@gmail.com>
 */
class Uploader
{
    /**
     * @var string
     */
    private $targetDirectory;

    /**
     * @param string  $targetDirectory
     */
    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    /**
     * @param UploadedFile $file
     * @param int          $height
     * @param int          $width
     *
     * @return string
     */
    public function upload(UploadedFile $file, $height, $width)
    {
        /**
         * @todo Imagine should be injected as a service through constructor argument
         */
        $imagine = new Imagine();
        $image = $imagine->open($file->getRealPath());

        $targetFileName = md5(uniqid()).'.'.$file->guessClientExtension();
        $targetPath = $this->targetDirectory.'/'.$targetFileName;

        $image
            ->resize(new Box($width, $height))
            ->save($targetPath)
        ;

        return $targetFileName;
    }
}
